from flask_security import MongoEngineUserDatastore
from data_models.courses.skill import Skill
from data_models.courses.course import Course


def recreate_database(user_datastore: MongoEngineUserDatastore):
    recreate_skills()
    admin_role = role_admin(user_datastore)
    student_role = role_student(user_datastore)
    instructor_role = role_instructor(user_datastore)
    training_supervisor_role = role_training_supervisor(user_datastore)

    create_admin_users(user_datastore, admin_role)
    create_student_users(user_datastore, student_role)
    create_instructor_users(user_datastore, instructor_role)
    create_training_supervisor_users(user_datastore, training_supervisor_role)

    create_courses()
    # print(user_datastore.find_user(email="matt@nobien.net").has_role(student_role))


def recreate_skills():
    list_skills = ["English", "Kubernetes", "Symfony"]
    for a_skill in list_skills:
        Skill(name=a_skill).save()


def role_admin(user_datastore: MongoEngineUserDatastore):
    return user_datastore.find_or_create_role(name="admin", description="Administrator")


def role_student(user_datastore: MongoEngineUserDatastore):
    return user_datastore.find_or_create_role(
        name="student", description="Student at school"
    )


def role_instructor(user_datastore: MongoEngineUserDatastore):
    return user_datastore.find_or_create_role(
        name="instructor",
        description="Instructor at school",
    )


def role_training_supervisor(user_datastore: MongoEngineUserDatastore):
    return user_datastore.find_or_create_role(
        name="training_supervisor",
        description="Worker that has also the job of training a student",
    )


def create_admin_users(user_datastore: MongoEngineUserDatastore, admin_role):
    if user_datastore.find_user(email="admin@administration.com") == None:
        user_datastore.create_user(
            email="admin@administration.com",
            password="imadmin",
            lastname="my admin",
            firstname="ad",
            roles=[admin_role],
        )


def create_student_users(user_datastore: MongoEngineUserDatastore, student_role):
    if user_datastore.find_user(email="matt@nobien.net") == None:
        user_datastore.create_user(
            email="matt@nobien.net",
            password="password",
            lastname="ttam",
            firstname="matt",
            roles=[student_role],
        )


def create_instructor_users(user_datastore: MongoEngineUserDatastore, instructor_role):
    if user_datastore.find_user(email="instructor@school.com") == None:
        user_datastore.create_user(
            email="instructor@school.com",
            password="improf",
            lastname="prof",
            firstname="le",
            roles=[instructor_role],
        )


def create_training_supervisor_users(
    user_datastore: MongoEngineUserDatastore, training_supervisor_role
):
    if user_datastore.find_user(email="supervisor@lambda.com") == None:
        user_datastore.create_user(
            email="supervisor@lambda.com",
            password="imsupervisor",
            lastname="visor",
            firstname="super",
            roles=[training_supervisor_role],
        )


def create_courses():
    dict_courses = {
        "English": "English 101",
        "Kubernetes": "Intro to Kubernetes",
        "Symfony": "Advanced Symfony",
    }
    for a_course_key in dict_courses:
        # Course(
        #    name=dict_courses[a_course_key], ref_skill=Skill.objects(name=a_course_key)[0]
        # ).save()
        print(Skill.objects(name=a_course_key)[0])
