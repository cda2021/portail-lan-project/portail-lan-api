import configparser
from flask import Flask, redirect, jsonify
from flask_security import (
    Security,
    MongoEngineUserDatastore,
    login_required,
    logout_user,
)
from flask_wtf.csrf import generate_csrf
from flask_restx import Api
from database.models.roles.role import Role
from database.models.users.user import User

# from init.before_first_request import recreate_database
from database.db import initialize_db, initialize_db_roles
from resources.routes import initialize_routes

# reading config
conf = configparser.ConfigParser()
conf.read("config.ini")

conf_db = conf["config"]
# Create app
app = Flask(__name__)
app.config["DEBUG"] = True
app.config["SECRET_KEY"] = "super-secret"
app.config["SECURITY_PASSWORD_SALT"] = "t-D64#Z=rFbqyhD5+55inXZm"
app.config["WTF_CSRF_ENABLED"] = False
# MongoDB Config
app.config["MONGODB_SETTINGS"] = {
    "host": f"mongodb://{conf_db['db_user']}:{conf_db['db_pass']}@127.0.0.1:27017/portail-lan-db?authSource=admin",
    "connect": True,
}
# Adding restful
api = Api(app, version="1.0", title="portail LAN API", description="API de portail LAN")
# api.namespace()
# Create database connection object
# db = MongoEngine(app)
db = initialize_db(app)
initialize_routes(api)

# Setup Flask-Security
user_datastore = MongoEngineUserDatastore(db, User, Role)
initialize_db_roles(user_datastore)
security = Security(app, user_datastore)


@app.after_request
def set_xsrf_cookie(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Methods", "*")
    response.headers.add(
        "Access-Control-Allow-Headers",
        "Content-Type,Authorization,Authentication-Token",
    )
    response.set_cookie("CSRF-TOKEN", generate_csrf())
    return response


@app.route("/discover", methods=["GET"])
@login_required
def discover():
    return jsonify({"status": "connected"})


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect("/")


"""
# Create a user to test with
@api.before_first_request
def create_user():
    recreate_database(user_datastore)


# Views
@api.route("/register", methods=["POST"])
def add_user():
    rq_data = json.loads(request.data)
    user = User(
        email=rq_data["email"],
        password=rq_data["password"],
        lastname=rq_data["lastname"],
        firstname=rq_data["firstname"],
    )
    user.save()
    return jsonify(user.to_json())


@api.route("/", methods=["GET"])
@login_required
def home():
    return jsonify({"status": "connected"})

"""
if __name__ == "__main__":
    app.run()
