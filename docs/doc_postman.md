# Documentation Postman

1. Postman nous permets de tester les routes de notre API.
1. Téléchargez le logiciel postman [à ce lien](https://www.postman.com/downloads/)
1. Importez la collection Postman avec le bouton import dans l'application
    ![](img/postman_import.png)
1. Importer la collection se trouvant dans ```postman/portail-lan-api.postman_collection.json```
1. La collection importée devrait se trouver dans l'onglet collection sur la gauche de postman, cliquez dessus et vous trouverez les routes suivantes:
    ![](img/postman_routes.png)
1. Les routes sont toutes testables à conditions d'utiliser les ID inscrits en base de données.
1. Vous pouvez inscrire des données de tests en faisant les requêtes POST (et en réutilisant les id retournés) dans l'ordre suivant :
    1. new user
    1. new roadmap
    1. new skill->new course(avec l'id précédent)->new schedule(avec minimum l'id précédent)->new promotion(avec l'id du user + l'id du schedule)
    1. new company(avec l'id de 2 users)
