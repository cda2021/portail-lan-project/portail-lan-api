# Documentation de l'api portail-lan-api

## Quickstart

1. Utilisez un Python avec comme version > 3.9.
1. Installez le gestionnaire d'environnement virtuels ```pipenv``` avec la commande ```pip install pipenv```
1. installez l'environnement virtuel : ```pipenv install```
1. Activez votre environnement virtuel : ```pipenv shell```
1. Lancez l'api (après avoir démarré le conteneur docker de la database). ```python api.py```
1. L'api est disponible à l'adresse http://localhost:5000/
1. Grâce à swagger dans le namespace ```default``` vous trouverez les routes de l'api.

## Installation de Python

1. Vous devez vous assurer de possèder un python > 3.9, si vous ne possèdez pas python veuillez le télécharger puis l'installer [à ce lien](https://www.python.org/downloads/)
1. Lors de l'installation (surtout sur windows) faites bien attention à ajouter python aux variables d'environnement, ce qui non permettra d'appeler son alias sans avoir à utiliser le chemin jusqu'à l'éxecutable.
1. Une fois python de téléchargé & installé, vous pouvez vérifier en lançant dans un terminal la commande suivante ```python --version```.

## Installation et fonctionnement de Pipenv

1. L'installation de Python est requise
1. Avec le gestionnaire de packages ```pip``` on installe le package pipenv qui va nous permettre de créer des environnements virtuels. Ces environnements sont stockés dans un dossier caché au niveau du dossier ```user``` de l'utilisateur actuel.
1. ```pipenv``` va nous permettre de créer des environnements virtuels afin de ne pas polluer le python et les packages globaux de Python.
1. Ensuite il faudra se placer au niveau du projet car ```pipenv``` utilise les fichier ```Pipfile```et ```Pipfile.lock``` afin de récupérer les packages. La commande ```pipenv install``` détecte automatiquement ces fichiers et installe les dépendances du projet automatiquement dans un environnement virtuel créé avec a une copie du Python installé.
1. Afin d'utiliser cet environnement vous devez activer votre environnement virtuel : ```pipenv shell```. Lorsque l'environnement est activé le python global et ses commandes sont remplacées par le python de l'environnement virtuel.

## Lancement de l'API

1. Une fois les étapes précédentes réalisées il ne suffit plus que de faire la commande ```python api.py``` dans le répertoire actuel. Le terminal actuel devrait alors vous informer que flask est lancé.
1. L'api est alors disponible à l'adresse suivante : http://localhost:5000/
1. Comme l'API utilise [swagger](https://swagger.io/) sur la route principale on retrouvera l'interface de ce dernier.
1. Les routes de l'API sont listées dans les namespace par défaut ```default``` en cliquant dessus.