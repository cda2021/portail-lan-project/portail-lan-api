# Documentation docker

Afin de pouvoir possèder la database mongodb vous devez possèder Docker [lien téléchargement](https://www.docker.com/get-started) (veuillez choisir la version de docker desktop correspondant à votre OS).

Une fois Docker téléchargé et installé il vous faudra vous positionner dans le dossier ```docker```. Dans un terminal écrivez la commande suivante : ```docker-compose up```

```docker-compose up``` fait appel au fichier docker-compose.yml et crée des ```conteneurs``` qui sont des mini linux (Docker utilise des images de Linux Alpine qui fait de base 4 mo). Ces conteneurs vont, une fois créés, lancer la base de donnée ```mongodb``` et l'utilitaire ```mongoexpress```.

```Attention, il est possible de lancer la commande docker-compose up avec l'attribut "-d" afin de l'executer en tâche de fond. Il est déconseillé de faire cela car on n'a pas de vision sur l'état des conteneurs à leur création. ```

```Cependant en lançant la commande de la sorte il faudra laisser la console utilisée ouverte car elle maintient le serveur. La supprimer stoppe les conteneurs.```

Une fois l'étape docker terminée vous pouvez passer à la configuration de l'api ou vous pouvez commencer à utiliser ```mongoexpress``` disponible à l'adresse http://localhost:8081 qui permet de visionner la database mongodb.
