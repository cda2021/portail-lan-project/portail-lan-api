from flask_restx import Resource
from flask import Response, request
from mongoengine_goodjson import Document


class BasicGetRoute(Resource):
    def get(self, document_object: Document):
        for an_object in document_object.objects:
            an_object.to_json()
        return Response(
            document_object.objects().to_json(), mimetype="application/json", status=200
        )


class BasicIdRoutes(Resource):
    def get(self, document_object: Document, id: str):
        return Response(
            document_object.objects.get(id=id).to_json(),
            mimetype="application/json",
            status=200,
        )

    def put(self, document_object: Document, id: str):
        body = request.get_json()
        if body == None:
            return {f"json values awaited": True}, 400
        try:
            document_object.objects.get(id=id).update(**body)
        except:
            return {f"incorrect json": True}, 400
        return {f"{document_object.id} updated": True}, 200

    def delete(self, document_object: Document, id: str):
        document_object.objects.get(id=id).delete()
        return {f"{document_object.id} deleted": True}, 200
