from flask import Response, request
from database.models.company.company import Company
from database.models.users.user import User
from resources.DAO.methods.basic_methods import BasicGetRoute, BasicIdRoutes


class CompaniesApi(BasicGetRoute):
    def get(self):
        return super().get(Company)

    def post(self):
        body = request.get_json(force=True)
        try:
            if "ref_student" in body:
                body["ref_student"] = User.objects.get(id=body["ref_student"])
            if "ref_training_supervisor" in body:
                body["ref_training_supervisor"] = User.objects.get(
                    id=body["ref_training_supervisor"]
                )
            company = Company(**body).save()
        except:
            return {"Fail to save Company": True}, 400
        return Response(company.to_json(), mimetype="application/json", status=200)


class CompanyApi(BasicIdRoutes):
    def get(self, id: str):
        return super().get(Company, id)

    def put(self, id):
        return super().put(Company, id)

    def delete(self, id):
        return super().delete(Company, id)
