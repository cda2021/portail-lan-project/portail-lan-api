from flask import Response, request
from database.models.courses.evaluate import Evaluate
from database.models.courses.skill import Skill
from database.models.users.user import User
from resources.DAO.methods.basic_methods import BasicGetRoute, BasicIdRoutes


class EvaluatesApi(BasicGetRoute):
    def get(self):
        return super().get(Evaluate)

    def post(self):
        body = request.get_json(force=True)
        try:
            body["ref_skill"] = Skill.objects.get(id=body["ref_skill"])
            body["ref_student"] = User.objects.get(id=body["ref_student"])
            if "ref_training_supervisor" in body and "ref_instructor" in body:
                return {
                    "either instructor and supervisor can evaluate, not both": True
                }, 400
            if "ref_training_supervisor" in body:
                body["ref_training_supervisor"] = User.objects.get(
                    id=body["ref_training_supervisor"]
                )

            if "ref_instructor" in body:
                body["ref_instructor"] = User.objects.get(id=body["ref_instructor"])
            course = Evaluate(**body).save()
        except:
            return {"Fail to save Evaluate": True}, 400
        return Response(course.to_json(), mimetype="application/json", status=200)


class EvaluateApi(BasicIdRoutes):
    def get(self, id: str):
        return super().get(Evaluate, id)

    def put(self, id):
        return super().put(Evaluate, id)

    def delete(self, id):
        return super().delete(Evaluate, id)
