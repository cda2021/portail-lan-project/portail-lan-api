from email.message import EmailMessage
import smtplib

from flask import Response, request, render_template
from database.models.users.user import User
from resources.DAO.methods.basic_methods import BasicGetRoute, BasicIdRoutes

# from flask_restx import fields, marshal_with


class UsersApi(BasicGetRoute):
    def get(self):
        return super().get(User)

    def post(self):
        body = request.get_json(force=True)
        try:
            user = User(**body).save()
        except:
            return {"Fail to save User": True}, 400

        msg = EmailMessage()
        msg.set_content(
            render_template(
                "mail.html",
                firstname=body["firstname"],
                email=body["email"],
                password=body["password"],
            )
        )
        msg.set_type(type="text/html")
        msg["Subject"] = "The subject"
        msg["From"] = "contact@aforp.com"
        msg["To"] = body["email"]

        s = smtplib.SMTP("localhost:1025")
        s.send_message(msg)
        s.quit()

        return Response(user.to_json(), mimetype="application/json", status=200)


class UserApi(BasicIdRoutes):
    def get(self, id: str):
        return super().get(User, id)

    def put(self, id):
        return super().put(User, id)

    def delete(self, id):
        return super().delete(User, id)
