from flask import Response, request
from database.models.courses.skill import Skill
from resources.DAO.methods.basic_methods import BasicGetRoute, BasicIdRoutes


class SkillsApi(BasicGetRoute):
    def get(self):
        return super().get(Skill)

    def post(self):
        body = request.get_json(force=True)
        try:
            skill = Skill(**body).save()
        except:
            return {"Fail to save Skill": True}, 400
        return Response(skill.to_json(), mimetype="application/json", status=200)


class SkillApi(BasicIdRoutes):
    def get(self, id: str):
        return super().get(Skill, id)

    def put(self, id):
        return super().put(Skill, id)

    def delete(self, id):
        return super().delete(Skill, id)
