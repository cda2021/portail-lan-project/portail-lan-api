from flask import Response, request
from mongoengine_goodjson.fields import follow_reference
from database.models.courses.promotion import Promotion
from database.models.users.user import User
from resources.DAO.methods.basic_methods import BasicGetRoute, BasicIdRoutes
from database.models.courses.schedule import Schedule

# from flask_restx import fields, marshal_with


class PromotionsApi(BasicGetRoute):
    def get(self):
        return super().get(Promotion)

    def post(self):
        body = request.get_json(force=True)
        if "list_student" not in body or body["list_student"] == None:
            return {"required key": "list_student"}, 400
        try:
            list_student = body["list_student"]
            body.pop("list_student", None)
            print(list_student)
            body["ref_list_student"] = []
            for a_student in list_student:
                body["ref_list_student"].append(User.objects.get(id=a_student))
            if "ref_schedule" in body:
                body["ref_schedule"] = Schedule.objects.get(id=body["ref_schedule"])
            promotion = Promotion(**body).save()
        except:
            return {"Fail to save Promotion": True}, 400
        return Response(
            promotion.to_json(follow_reference=True),
            mimetype="application/json",
            status=200,
        )


class PromotionApi(BasicIdRoutes):
    def get(self, id: str):
        return super().get(Promotion, id)

    def put(self, id):
        return super().put(Promotion, id)

    def delete(self, id):
        return super().delete(Promotion, id)
