from flask import Response, request
from database.models.courses.course import Course
from database.models.courses.skill import Skill
from resources.DAO.methods.basic_methods import BasicGetRoute, BasicIdRoutes


class CoursesApi(BasicGetRoute):
    def get(self):
        return super().get(Course)

    def post(self):
        body = request.get_json(force=True)
        try:
            body["ref_skill"] = Skill.objects.get(id=body["ref_skill"])
            course = Course(**body).save()
        except:
            return {"Fail to save Course": True}, 400
        return Response(course.to_json(), mimetype="application/json", status=200)


class CourseApi(BasicIdRoutes):
    def get(self, id: str):
        return super().get(Course, id)

    def put(self, id):
        return super().put(Course, id)

    def delete(self, id):
        return super().delete(Course, id)
