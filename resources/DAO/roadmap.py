from flask import Response, request
from database.models.courses.roadmap import Roadmap
from database.models.courses.skill import Skill
from resources.DAO.methods.basic_methods import BasicGetRoute, BasicIdRoutes


class RoadmapsApi(BasicGetRoute):
    def get(self):
        return super().get(Roadmap)

    def post(self):
        body = request.get_json(force=True)
        try:
            roadmap = Roadmap(**body).save()
        except:
            return {"Fail to save Roadmap": True}, 400
        return Response(roadmap.to_json(), mimetype="application/json", status=200)


class RoadmapApi(BasicIdRoutes):
    def get(self, id: str):
        return super().get(Roadmap, id)

    def put(self, id):
        return super().put(Roadmap, id)

    def delete(self, id):
        return super().delete(Roadmap, id)
