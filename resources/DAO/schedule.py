from flask import Response, request
from database.models.courses.schedule import Schedule
from database.models.courses.course import Course
from flask_restx import Resource
from resources.DAO.methods.basic_methods import BasicGetRoute, BasicIdRoutes
from database.models.courses.skill import Skill
from resources.DAO.course import CoursesApi


class SchedulesApi(BasicGetRoute):
    def get(self):
        return super().get(Schedule)

    def post(self):
        body = request.get_json(force=True)
        try:
            list_cours = body["ref_list_cours"]
            print(list_cours)
            body["ref_list_cours"] = []
            for a_course in list_cours:
                body["ref_list_cours"].append(Course.objects.get(id=a_course))
            schedule = Schedule(**body).save()
        except:
            return {"Fail to save Schedule": True}, 400
        # courses = schedule.ref_list_cours
        # return {'schedule_id': str(schedule.id)}, 200
        return Response(schedule.to_json(), mimetype="application/json", status=200)


class ScheduleApi(BasicIdRoutes):
    def get(self, id: str):
        return super().get(Schedule, id)

    def put(self, id):
        return super().put(Schedule, id)

    def delete(self, id):
        return super().delete(Schedule, id)


class ScheduleCourseLink(Resource):
    def post(self, schedule_id):
        body = request.get_json()
        course = body
        try:
            if "ref_skill" in course:
                course["ref_skill"] = Skill.objects.get(id=course["ref_skill"])
            course = Course(**course).save()
        except:
            return {"Fail to save Course": True}, 400
        schedule = Schedule.objects.get(id=schedule_id)
        schedule.ref_list_cours.append(course)
        schedule.save()
        return {"schedule": str(schedule)}, 200
