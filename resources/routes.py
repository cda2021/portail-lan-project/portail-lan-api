from .DAO.user import UsersApi, UserApi
from .DAO.skill import SkillsApi, SkillApi
from .DAO.course import CoursesApi, CourseApi
from .DAO.schedule import SchedulesApi, ScheduleApi, ScheduleCourseLink
from .DAO.roadmap import RoadmapsApi, RoadmapApi
from .DAO.promotion import PromotionsApi, PromotionApi
from .DAO.company import CompaniesApi, CompanyApi
from .DAO.evaluate import EvaluatesApi, EvaluateApi


def initialize_routes(api):
    api.add_resource(UsersApi, "/api/users")
    api.add_resource(UserApi, "/api/users/<id>")
    api.add_resource(SkillsApi, "/api/skills")
    api.add_resource(SkillApi, "/api/skills/<name>")
    api.add_resource(CoursesApi, "/api/courses")
    api.add_resource(CourseApi, "/api/courses/<name>")
    api.add_resource(SchedulesApi, "/api/schedules")
    api.add_resource(ScheduleApi, "/api/schedules/<schedule_id>")
    api.add_resource(ScheduleCourseLink, "/api/schedules/<schedule_id>/add")
    api.add_resource(RoadmapsApi, "/api/roadmaps")
    api.add_resource(RoadmapApi, "/api/roadmaps/<label>")
    api.add_resource(PromotionsApi, "/api/promotions")
    api.add_resource(PromotionApi, "/api/promotions/<id>")
    api.add_resource(CompaniesApi, "/api/companies")
    api.add_resource(CompanyApi, "/api/companies/<id>")
    api.add_resource(EvaluatesApi, "/api/evaluates")
    # api.add_resource(EvaluateApi, "/api/compagnies/<id>")
