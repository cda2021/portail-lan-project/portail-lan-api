from flask import json

from api import return_api

urlvars = False  # Build query strings in URLs
swagger = True  # Export Swagger specifications
data = return_api().as_postman(urlvars=urlvars, swagger=swagger)
print(json.dumps(data))
