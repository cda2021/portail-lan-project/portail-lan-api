# portail-lan-api

API of portail-lan project

## Configuration Docker

Voir la config [disponible ici](docs/doc_docker.md)

## Configuration API Flask

Voir la config [disponible ici](docs/doc_api.md)

## Configuration Postman

Voir la config [disponible ici](docs/doc_postman.md)
