from mongoengine import connect


def initialize_db(app):
    return connect(host=app.config["MONGODB_SETTINGS"]["host"])


def initialize_db_roles(user_datastore):
    user_datastore.find_or_create_role(name="admin", description="Administrator")
    user_datastore.find_or_create_role(name="student", description="Student at school")
    user_datastore.find_or_create_role(
        name="instructor", description="Instructor at school"
    )
    user_datastore.find_or_create_role(
        name="training_supervisor",
        description="Worker that has also the job of training a student",
    )
