from database.models.courses.course import Course

# from database.models.courses.promotion import Promotion
from mongoengine import (
    ReferenceField,
    ListField,
)
from mongoengine_goodjson import Document, FollowReferenceField
from mongoengine_goodjson.fields import FollowReferenceField


class Schedule(Document):
    ref_list_cours = ListField(
        FollowReferenceField(Course, dbref=False, max_depth=-1), required=False
    )
    # ref_promotion = ReferenceField(Promotion, dbref=False, required=True)
