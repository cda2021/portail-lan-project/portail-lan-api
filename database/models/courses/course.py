from mongoengine_goodjson import FollowReferenceField, Document
from mongoengine import StringField, IntField
from database.models.courses.skill import Skill


class Course(Document):
    name = StringField(max_length=70, required=True)
    ref_skill = FollowReferenceField(Skill, dbref=False, required=False, max_depth=-1)
    date_course = IntField()
