from mongoengine import (
    StringField,
)
from mongoengine_goodjson import Document


class Skill(Document):
    name = StringField(max_length=70, required=True, unique=True)
