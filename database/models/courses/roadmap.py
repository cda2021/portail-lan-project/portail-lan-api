from mongoengine import (
    StringField,
)
from mongoengine_goodjson import Document


class Roadmap(Document):
    label = StringField(max_length=255, required=True)
