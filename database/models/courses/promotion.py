from mongoengine import (
    StringField,
    ListField,
)
from mongoengine_goodjson import FollowReferenceField, Document
from database.models.users.user import User
from database.models.courses.schedule import Schedule


class Promotion(Document):
    name = StringField(max_length=70, required=True)
    ref_list_student = ListField(FollowReferenceField(User, dbref=False), required=True)
    ref_schedule = FollowReferenceField(
        Schedule, dbref=False, required=False, max_depth=-1
    )
