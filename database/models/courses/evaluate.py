from mongoengine import StringField, DateTimeField
from mongoengine_goodjson import Document, FollowReferenceField
from database.models.courses.skill import Skill
from database.models.users.user import User


class Evaluate(Document):
    graduation = StringField(max_length=70, required=True)
    graduated_at = DateTimeField()
    ref_skill = FollowReferenceField(Skill, dbref=False, required=True)
    ref_student = FollowReferenceField(User, dbref=False, required=True)
    ref_training_supervisor = FollowReferenceField(User, dbref=False)
    ref_instructor = FollowReferenceField(User, dbref=False)
