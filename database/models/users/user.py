from flask_security import UserMixin
from mongoengine import (
    StringField,
    BooleanField,
    DateTimeField,
    ListField,
)
from mongoengine_goodjson import FollowReferenceField, Document
from database.models.roles.role import Role
from database.models.courses.roadmap import Roadmap


class User(Document, UserMixin):
    email = StringField(max_length=255, required=True, unique=True)
    lastname = StringField(max_length=70, required=True)
    firstname = StringField(max_length=70, required=True)
    password = StringField(max_length=255, required=True)
    role = StringField(max_length=255, required=False, default="student")
    active = BooleanField(default=True)
    confirmed_at = DateTimeField()
    roles = ListField(FollowReferenceField(Role, dbref=False), default=[])
    ref_roadmap = FollowReferenceField(Roadmap, dbref=False, required=False)
