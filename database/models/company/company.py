from mongoengine import (
    StringField,
)
from mongoengine_goodjson import Document, FollowReferenceField
from database.models.users.user import User


class Company(Document):
    name = StringField(max_length=70, required=True)
    siret = StringField(max_length=70, required=True)
    address = StringField(max_length=255, required=True)
    ref_student = FollowReferenceField(User, dbref=False)
    ref_training_supervisor = FollowReferenceField(User, dbref=False)
