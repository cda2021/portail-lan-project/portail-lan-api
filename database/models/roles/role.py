from flask_security import RoleMixin
from mongoengine import StringField
from mongoengine_goodjson import Document, EmbeddedDocument


class Role(Document, RoleMixin):
    name = StringField(max_length=80, unique=True, required=True)
    description = StringField(max_length=255)
